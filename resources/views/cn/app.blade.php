<!DOCTYPE html>
<!-- saved from url=(0030)https://careernetwork.amro.io/ -->
<html lang="en-US" class=" video csspointerevents no-touchevents flexbox objectfit object-fit backgroundcliptext">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<meta name="twitter:widgets:csp" content="on">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="https://careernetwork.amro.io/xmlrpc.php">

		<title>Career Network</title>
		<link rel="dns-prefetch" href="https://fonts.googleapis.com/">
		<link rel="dns-prefetch" href="https://s.w.org/">
		<link rel="alternate" type="application/rss+xml" title="Career Network » Feed" href="https://careernetwork.amro.io/feed/">
		<link rel="alternate" type="application/rss+xml" title="Career Network » Comments Feed" href="https://careernetwork.amro.io/comments/feed/">
		<script src="themes/cn/js/sdk2.js" async=""></script>
		<script id="facebook-jssdk" src="themes/cn/js/sdk.js"></script>
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/careernetwork.amro.io\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.6"}};
			/*! This file is auto-generated */
			!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<script src="themes/cn/js/wp-emoji-release.min.js?ver=5.4.6" type="text/javascript" defer=""></script>
		<style type="text/css">
			img.wp-smiley,
			img.emoji {
				display: inline !important;
				border: none !important;
				box-shadow: none !important;
				height: 1em !important;
				width: 1em !important;
				margin: 0 .07em !important;
				vertical-align: -0.1em !important;
				background: none !important;
				padding: 0 !important;
			}
		</style>
		<link rel="stylesheet" id="zn_all_g_fonts-css" href="themes/cn/css/style.css" type="text/css" media="all">
		<link rel="stylesheet" id="font-awesome-css" href="themes/cn/css/font-awesome.min.css?ver=4.7.0" type="text/css" media="all">
		<link rel="stylesheet" id="menu-icons-extra-css" href="themes/cn/css/extra.min.css?ver=0.12.2" type="text/css" media="all">
		<link rel="stylesheet" id="wp-block-library-css" href="themes/cn/css/style.min.css?ver=5.4.6" type="text/css" media="all">
		<link rel="stylesheet" id="kallyas-styles-css" href="themes/cn/css/style2.css?ver=4.17.3" type="text/css" media="all">
		<link rel="stylesheet" id="th-bootstrap-styles-css" href="themes/cn/css/bootstrap.min.css?ver=4.17.3" type="text/css" media="all">
		<link rel="stylesheet" id="th-theme-template-styles-css" href="themes/cn/css/template.min.css?ver=4.17.3" type="text/css" media="all">
		<link rel="stylesheet" id="zion-frontend-css" href="themes/cn/css/znb_frontend.css?ver=1.0.27" type="text/css" media="all">
		<link rel="stylesheet" id="175-layout.css-css" href="themes/cn/css/175-layout.css?ver=42b5c9c93f072787ac495d9e042fd559" type="text/css" media="all">
		<link rel="stylesheet" id="kallyas-addon-nav-overlay-css-css" href="themes/cn/css/styles.min.css?ver=1.0.10" type="text/css" media="all">
		<link rel="stylesheet" id="th-theme-print-stylesheet-css" href="themes/cn/css/print.css?ver=4.17.3" type="text/css" media="print">
		<link rel="stylesheet" id="th-theme-options-styles-css" href="themes/cn/css/zn_dynamic.css?ver=1569849410" type="text/css" media="all">
		<script type="text/javascript" src="themes/cn/js/jquery.min.js?ver=1.12.4-wp"></script>
		<script type="text/javascript" src="themes/cn/js/jquery-migrate.min.js?ver=1.4.1"></script>
		<!-- <link rel="https://api.w.org/" href="https://careernetwork.amro.io/wp-json/">
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://careernetwork.amro.io/xmlrpc.php?rsd">
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://careernetwork.amro.io/wp-includes/wlwmanifest.xml"> 
		-->
		<meta name="generator" content="Laravel 8">
		<!-- <link rel="canonical" href="https://careernetwork.amro.io/">
		<link rel="shortlink" href="https://careernetwork.amro.io/">
		<link rel="alternate" type="application/json+oembed" href="https://careernetwork.amro.io/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcareernetwork.amro.io%2F">
		<link rel="alternate" type="text/xml+oembed" href="https://careernetwork.amro.io/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcareernetwork.amro.io%2F&amp;format=xml"> -->
		<meta name="theme-color" content="#02a1ea">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			
			<!--[if lte IE 8]>
			<script type="text/javascript">
				var $buoop = {
					vs: {i: 10, f: 25, o: 12.1, s: 7, n: 9}
				};

				$buoop.ol = window.onload;

				window.onload = function () {
					try {
						if ($buoop.ol) {
							$buoop.ol()
						}
					}
					catch (e) {
					}

					var e = document.createElement("script");
					e.setAttribute("type", "text/javascript");
					e.setAttribute("src", "https://browser-update.org/update.js");
					document.body.appendChild(e);
				};
			</script>
			<![endif]-->

			<!-- for IE6-8 support of HTML5 elements -->
			<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
			
		<!-- Fallback for animating in viewport -->
		<noscript>
			<style type="text/css" media="screen">
				.zn-animateInViewport {visibility: visible;}
			</style>
		</noscript>
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link rel="icon" href="https://careernetwork.amro.io/wp-content/uploads/2019/08/fav.png" sizes="32x32">
		<link rel="icon" href="./images/fav.png" sizes="192x192">
		<link rel="apple-touch-icon" href="./images/fav.png">
		<meta name="msapplication-TileImage" content="./images/fav.png">
		<style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style><style type="text/css" data-fbcssmodules="css:fb.css.base css:fb.css.dialog css:fb.css.iframewidget css:fb.css.customer_chat_plugin_iframe">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
			.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_dialog_advanced{border-radius:8px;padding:10px}.fb_dialog_content{background:#fff;color:#373737}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{left:5px;right:auto;top:5px}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{height:100%;left:0;margin:0;overflow:visible;position:absolute;top:-10000px;transform:none;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{background:none;height:auto;min-height:initial;min-width:initial;width:auto}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{clear:both;color:#fff;display:block;font-size:18px;padding-top:20px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .4);bottom:0;left:0;min-height:100%;position:absolute;right:0;top:0;width:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_mobile .fb_dialog_iframe{position:sticky;top:0}.fb_dialog_content .dialog_header{background:linear-gradient(from(#738aba), to(#2c4987));border-bottom:1px solid;border-color:#043b87;box-shadow:white 0 1px 1px -1px inset;color:#fff;font:bold 14px Helvetica, sans-serif;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:linear-gradient(from(#4267B2), to(#2a4887));background-clip:padding-box;border:1px solid #29487d;border-radius:3px;display:inline-block;line-height:18px;margin-top:3px;max-width:85px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{background:none;border:none;color:#fff;font:bold 12px Helvetica, sans-serif;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #4a4a4a;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f5f6f7;border:1px solid #4a4a4a;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-position:50% 50%;background-repeat:no-repeat;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
			.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}
			.fb_mpn_mobile_landing_page_slide_out{animation-duration:200ms;animation-name:fb_mpn_landing_page_slide_out;transition-timing-function:ease-in}.fb_mpn_mobile_landing_page_slide_out_from_left{animation-duration:200ms;animation-name:fb_mpn_landing_page_slide_out_from_left;transition-timing-function:ease-in}.fb_mpn_mobile_landing_page_slide_up{animation-duration:500ms;animation-name:fb_mpn_landing_page_slide_up;transition-timing-function:ease-in}.fb_mpn_mobile_bounce_in{animation-duration:300ms;animation-name:fb_mpn_bounce_in;transition-timing-function:ease-in}.fb_mpn_mobile_bounce_out{animation-duration:300ms;animation-name:fb_mpn_bounce_out;transition-timing-function:ease-in}.fb_mpn_mobile_bounce_out_v2{animation-duration:300ms;animation-name:fb_mpn_fade_out;transition-timing-function:ease-in}.fb_customer_chat_bounce_in_v2{animation-duration:300ms;animation-name:fb_bounce_in_v2;transition-timing-function:ease-in}.fb_customer_chat_bounce_in_from_left{animation-duration:300ms;animation-name:fb_bounce_in_from_left;transition-timing-function:ease-in}.fb_customer_chat_bounce_out_v2{animation-duration:300ms;animation-name:fb_bounce_out_v2;transition-timing-function:ease-in}.fb_customer_chat_bounce_out_from_left{animation-duration:300ms;animation-name:fb_bounce_out_from_left;transition-timing-function:ease-in}.fb_customer_chat_bubble_animated_no_badge{box-shadow:0 3px 12px rgba(0, 0, 0, .15);transition:box-shadow 150ms linear}.fb_customer_chat_bubble_animated_no_badge:hover{box-shadow:0 5px 24px rgba(0, 0, 0, .3)}.fb_customer_chat_bubble_animated_with_badge{box-shadow:-5px 4px 14px rgba(0, 0, 0, .15);transition:box-shadow 150ms linear}.fb_customer_chat_bubble_animated_with_badge:hover{box-shadow:-5px 8px 24px rgba(0, 0, 0, .2)}.fb_invisible_flow{display:inherit;height:0;overflow-x:hidden;width:0}.fb_new_ui_mobile_overlay_active{overflow:hidden}@keyframes fb_mpn_landing_page_slide_in{0%{border-radius:50%;margin:0 24px;width:60px}40%{border-radius:18px}100%{margin:0 12px;width:100% - 24px}}@keyframes fb_mpn_landing_page_slide_in_from_left{0%{border-radius:50%;left:12px;margin:0 24px;width:60px}40%{border-radius:18px}100%{left:12px;margin:0 12px;width:100% - 24px}}@keyframes fb_mpn_landing_page_slide_out{0%{margin:0 12px;width:100% - 24px}60%{border-radius:18px}100%{border-radius:50%;margin:0 24px;width:60px}}@keyframes fb_mpn_landing_page_slide_out_from_left{0%{left:12px;width:100% - 24px}60%{border-radius:18px}100%{border-radius:50%;left:12px;width:60px}}@keyframes fb_mpn_landing_page_slide_up{0%{bottom:0;opacity:0}100%{bottom:24px;opacity:1}}@keyframes fb_mpn_bounce_in{0%{opacity:.5;top:100%}100%{opacity:1;top:0}}@keyframes fb_mpn_fade_out{0%{bottom:30px;opacity:1}100%{bottom:0;opacity:0}}@keyframes fb_mpn_bounce_out{0%{opacity:1;top:0}100%{opacity:.5;top:100%}}@keyframes fb_bounce_in_v2{0%{opacity:0;transform:scale(0, 0);transform-origin:bottom right}50%{transform:scale(1.03, 1.03);transform-origin:bottom right}100%{opacity:1;transform:scale(1, 1);transform-origin:bottom right}}@keyframes fb_bounce_in_from_left{0%{opacity:0;transform:scale(0, 0);transform-origin:bottom left}50%{transform:scale(1.03, 1.03);transform-origin:bottom left}100%{opacity:1;transform:scale(1, 1);transform-origin:bottom left}}@keyframes fb_bounce_out_v2{0%{opacity:1;transform:scale(1, 1);transform-origin:bottom right}100%{opacity:0;transform:scale(0, 0);transform-origin:bottom right}}@keyframes fb_bounce_out_from_left{0%{opacity:1;transform:scale(1, 1);transform-origin:bottom left}100%{opacity:0;transform:scale(0, 0);transform-origin:bottom left}}@keyframes fb_bounce_out_v2_mobile_chat_started{0%{opacity:1;top:0}100%{opacity:0;top:20px}}@keyframes fb_customer_chat_bubble_bounce_in_animation{0%{bottom:6pt;opacity:0;transform:scale(0, 0);transform-origin:center}70%{bottom:18pt;opacity:1;transform:scale(1.2, 1.2)}100%{transform:scale(1, 1)}}@keyframes slideInFromBottom{0%{opacity:.1;transform:translateY(100%)}100%{opacity:1;transform:translateY(0)}}@keyframes slideInFromBottomDelay{0%{opacity:0;transform:translateY(100%)}97%{opacity:0;transform:translateY(100%)}100%{opacity:1;transform:translateY(0)}}
		</style>
	</head>

<body class="home page-template-default page page-id-175 res1170 kl-skin--light" itemscope="itemscope" itemtype="https://schema.org/WebPage">
	<div class="login_register_stuff"></div><!-- end login register stuff -->		
		<div id="fb-root" class=" fb_reset">
			<div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
				<div>
				</div>
			</div>
		</div>
		<script>
			(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
			
	<div id="zn-nav-overlay" class="znNavOvr znNavOvr--layoutS1 znNavOvr--animation3 znNavOvr--theme-light">

		<div class="znNavOvr-inner is-empty">
			<div class="znNavOvr-menuWrapper"></div>
		</div>

		<a href="https://careernetwork.amro.io/#" class="znNavOvr-close znNavOvr-close--trSmall" id="znNavOvr-close">
			<span></span>
			<svg x="0px" y="0px" width="54px" height="54px" viewBox="0 0 54 54">
				<circle fill="transparent" stroke="#656e79" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
			</svg>
		</a>
	</div>

	<div id="page_wrapper">

		<header id="header" class="site-header  style12 cta_button    header--no-stick headerstyle-xs--image_color  sticky-resize headerstyle--default site-header--absolute nav-th--light sheader-sh--dark" role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
			<div class="site-header-wrapper sticky-top-area">
				<div class="site-header-top-wrapper topbar-style--custom  sh--light">
					<div class="siteheader-container container">
						<div class="fxb-row site-header-row site-header-top ">
							<div class="fxb-col fxb fxb-start-x fxb-center-y fxb-basis-auto site-header-col-left site-header-top-left">
								<ul class="sh-component social-icons sc--clean topnav navRight topnav-no-hdnav">
									<li class="topnav-li social-icons-li"><a href="https://www.facebook.com/ITRecruitmentza/" data-zniconfam="kl-social-icons" data-zn_icon="" target="_blank" class="topnav-item social-icons-item scheader-icon-" title="Facebook"></a></li>
									<li class="topnav-li social-icons-li"><a href="https://careernetwork.amro.io/#" data-zniconfam="kl-social-icons" data-zn_icon="" target="_blank" class="topnav-item social-icons-item scheader-icon-" title="LinkedIn"></a></li>
									<li class="topnav-li social-icons-li"><a href="https://careernetwork.amro.io/#" data-zniconfam="kl-social-icons" data-zn_icon="" target="_blank" class="topnav-item social-icons-item scheader-icon-" title="Twitter"></a></li>
								</ul>					
							</div>
							<div class="fxb-col fxb fxb-end-x fxb-center-y fxb-basis-auto site-header-col-right site-header-top-right">
								<div class="sh-component zn_header_top_nav-wrapper ">
									<span class="headernav-trigger js-toggle-class" data-target=".zn_header_top_nav-wrapper" data-target-class="is-opened"></span>
									<ul id="menu-top-menu-1" class="zn_header_top_nav topnav topnav-no-sc clearfix">
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-123"><a href="http://careernetwork.amro.io/getting-started/">RECRUITER SIGN IN</a></li>
									</ul>
								</div>
								<div id="search" class="sh-component header-search headsearch--min">
									<a href="https://careernetwork.amro.io/#" class="searchBtn header-search-button">
										<span class="glyphicon glyphicon-search kl-icon-white"></span>
									</a>
									<div class="search-container header-search-container">
										<form id="searchform" class="gensearch__form" action="https://careernetwork.amro.io/" method="get">
											<input id="s" name="s" value="" class="inputbox gensearch__input" type="text" placeholder="SEARCH ...">
											<button type="submit" id="searchsubmit" value="go" class="gensearch__submit glyphicon glyphicon-search"></button>
										</form>			
									</div>
								</div>
							</div>
						</div><!-- /.site-header-top -->
					</div>
				</div><!-- /.site-header-top-wrapper -->
				<div class="kl-top-header site-header-main-wrapper clearfix   header-no-bottom  sh--dark">
					<div class="container siteheader-container ">
						<div class="fxb-col fxb-basis-auto">
							<div class="fxb-row site-header-row site-header-main ">
								<div class="fxb-col fxb fxb-start-x fxb-center-y fxb-basis-auto fxb-grow-0 fxb-sm-half site-header-col-left site-header-main-left">
									<div id="logo-container" class="logo-container  hasHoverMe logosize--no zn-original-logo">
									<!-- Logo -->
									<h1 class="site-logo logo " id="logo"><a href="https://careernetwork.amro.io/" class="site-logo-anch"><img class="logo-img site-logo-img" src="https://careernetwork.amro.io/wp-content/uploads/2019/08/web_top_logo.png" width="219" alt="Career Network" title="My WordPress Blog"></a></h1>			<!-- InfoCard -->
								</div>
							</div>
							<div class="fxb-col fxb fxb-center-x fxb-center-y fxb-basis-auto fxb-grow-0 site-header-col-center site-header-main-center"></div>
								<div class="fxb-col fxb fxb-end-x fxb-center-y fxb-basis-auto fxb-sm-half site-header-col-right site-header-main-right">
									<div class="fxb-col fxb fxb-end-x fxb-center-y fxb-basis-auto fxb-sm-half site-header-main-right-top">
										<div class="sh-component main-menu-wrapper" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
											<div class="zn-res-menuwrapper">
												<a href="https://careernetwork.amro.io/#" class="zn-res-trigger zn-menuBurger zn-menuBurger--2--s zn-menuBurger--anim1" id="zn-res-trigger">
													<span></span>
													<span></span>
													<span></span>
												</a>
											</div><!-- end responsive menu -->
											<div id="main-menu" class="main-nav mainnav--overlay mainnav--active-uline mainnav--pointer-dash nav-mm--light zn_mega_wrapper ">
												<ul id="menu-job-search-menu" class="main-menu main-menu-nav zn_mega_menu "><li id="menu-item-126" class="main-menu-item menu-item menu-item-type-custom menu-item-object-custom menu-item-126  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="https://careernetwork.amro.io/#" class=" main-menu-link main-menu-link-top"><span>JOB SEARCH</span></a></li>
													<li id="menu-item-718" class="main-menu-item menu-item menu-item-type-custom menu-item-object-custom menu-item-718  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="https://careernetwork.amro.io/#" class=" main-menu-link main-menu-link-top"><span>COURSES SEEK</span></a></li>
													<li id="menu-item-720" class="main-menu-item menu-item menu-item-type-custom menu-item-object-custom menu-item-720  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="https://careernetwork.amro.io/#" class=" main-menu-link main-menu-link-top"><span>CAREER RESOURCES</span></a></li>
													<li id="menu-item-721" class="main-menu-item menu-item menu-item-type-custom menu-item-object-custom menu-item-721  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="https://careernetwork.amro.io/#" class=" main-menu-link main-menu-link-top"><span>CANDIDATE SEARCH</span></a></li>
													<li id="menu-item-454" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-454  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="https://careernetwork.amro.io/my-profile/" class=" main-menu-link main-menu-link-top"><span>SIGN IN</span></a></li>
												</ul>
											</div>		
										</div>
										<!-- end main_menu -->
										<a href="http://careernetwork.amro.io/cv-builder/" class="sh-component ctabutton cta-button-0 kl-cta-custom btn btn-fullcolor btn-custom-color  zn_dummy_value cta-icon--before btn--rounded" target="_self" itemprop="url"><span>BUILD YOUR CV</span></a>		
									</div>		
								</div>
							</div><!-- /.site-header-main -->
						</div>
					</div><!-- /.siteheader-container -->
				</div><!-- /.site-header-main-wrapper -->
			</div><!-- /.site-header-wrapper -->
		</header>

		<div class="zn_pb_wrapper clearfix zn_sortable_content" data-droplevel="0">	

		@yield('content')

		</div>
		


		<div class="znpb-footer-smart-area">
				<section class="zn_section eluid82c261af section-sidemargins zn_section--relative section--no " id="eluid82c261af">
					<div class="zn-bgSource ">
						<div class="zn-bgSource-overlay" style=" background-image: -moz-linear-gradient( 129deg, rgb(16,108,160) 0%, rgb(33,145,173) 99%); background-image: -webkit-linear-gradient( 129deg, rgb(16,108,160) 0%, rgb(33,145,173) 99%); background-image: -ms-linear-gradient( 129deg, rgb(16,108,160) 0%, rgb(33,145,173) 99%);"></div>
					</div>
					<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">
						<div class="row ">
							<div class="eluidcf4b0783 col-md-3 col-sm-3   znColumnElement" id="eluidcf4b0783">
								<div class="znColumnElement-innerWrapper-eluidcf4b0783 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
									<div class="znColumnElement-innerContent">					
										<div class="image-boxes imgbox-simple eluid05df677e ">
											<div class="image-boxes-holder imgboxes-wrapper u-mb-0  ">
												<div class="image-boxes-img-wrapper img-align-center">
													<img class="image-boxes-img img-responsive " src="http://careernetwork.amro.io/wp-content/uploads/2019/08/Untitled-10.png" alt="" title="Untitled-10">
												</div>
											</div>
										</div>				
									</div>
								</div>
							</div>
							<div class="eluid86584de3 col-md-3 col-sm-3 znColumnElement" id="eluid86584de3">
								<div class="znColumnElement-innerWrapper-eluid86584de3 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
									<div class="znColumnElement-innerContent">					
										<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--  tbk-icon-pos--after-title eluid1834a96f ">
											<h4 class="tbk__title" itemprop="headline">Our Location</h4>
											<div class="tbk__subtitle" itemprop="alternativeHeadline">Menlo Park, <br>Pretoria</div>
										</div>				
									</div>
								</div>
							</div>
							<div class="eluid33bab954 col-md-3 col-sm-3 znColumnElement" id="eluid33bab954">
							<div class="znColumnElement-innerWrapper-eluid33bab954 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
								<div class="znColumnElement-innerContent">					
									<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--  tbk-icon-pos--after-title eluided076af4 "><h4 class="tbk__title" itemprop="headline">Contact</h4>
										<div class="tbk__subtitle" itemprop="alternativeHeadline">
											(082) 540 8464<br> info@careernetwork.co.za</div>
										</div>				
									</div>
								</div>
							</div>
							<div class="eluid7dbec239 col-md-3 col-sm-3 znColumnElement" id="eluid7dbec239">
								<div class="znColumnElement-innerWrapper-eluid7dbec239 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">
									<div class="znColumnElement-innerContent">					
										<div class="elm-socialicons eluid9c3297a4  text-right sc-icon--right elm-socialicons--light element-scheme--light">
											<ul class="elm-social-icons sc--clean sh--rounded sc-lay--normal clearfix">
												<li class="elm-social-icons-item"><a href="https://www.facebook.com/ITRecruitmentza/" class="elm-sc-link elm-sc-icon-0" target="_blank" rel="noopener" itemprop="url"><span class="elm-sc-icon " data-zniconfam="kl-social-icons" data-zn_icon=""></span></a><div class="clearfix"></div></li>
												<li class="elm-social-icons-item"><a href="https://careernetwork.amro.io/#" class="elm-sc-link elm-sc-icon-1" target="_self" itemprop="url"><span class="elm-sc-icon " data-zniconfam="kl-social-icons" data-zn_icon=""></span></a><div class="clearfix"></div></li>
												<li class="elm-social-icons-item"><a href="https://careernetwork.amro.io/#" class="elm-sc-link elm-sc-icon-2" target="_self" itemprop="url"><span class="elm-sc-icon " data-zniconfam="kl-social-icons" data-zn_icon=""></span></a><div class="clearfix"></div></li>
											</ul>
										</div>				
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div><!-- end page_wrapper -->

		<a href="https://careernetwork.amro.io/#" id="totop" class="u-trans-all-2s js-scroll-event" data-forch="300" data-visibleclass="on--totop">TOP</a>

		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/themes/kallyas/js/plugins.min.js?ver=4.17.3"></script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/themes/kallyas/addons/scrollmagic/scrollmagic.js?ver=4.17.3"></script>
		<script type="text/javascript">
		/* <![CDATA[ */
		var zn_do_login = {"ajaxurl":"\/wp-admin\/admin-ajax.php","add_to_cart_text":"Item Added to cart!"};
		var ZnThemeAjax = {"ajaxurl":"\/wp-admin\/admin-ajax.php","zn_back_text":"Back","zn_color_theme":"light","res_menu_trigger":"1199","top_offset_tolerance":"","logout_url":"https:\/\/careernetwork.amro.io\/wp-login.php?action=logout&redirect_to=https%3A%2F%2Fcareernetwork.amro.io&_wpnonce=375fab8263"};
		var ZnSmoothScroll = {"type":"yes","touchpadSupport":"no"};
		/* ]]> */
		</script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/themes/kallyas/js/znscript.min.js?ver=4.17.3"></script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/themes/kallyas/addons/smooth_scroll/SmoothScroll.min.js?ver=4.17.3"></script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/themes/kallyas/addons/slick/slick.min.js?ver=4.17.3"></script>
		<script type="text/javascript">
		/* <![CDATA[ */
		var ZionBuilderFrontend = {"allow_video_on_mobile":""};
		/* ]]> */
		</script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/themes/kallyas/framework/zion-builder/dist/znpb_frontend.bundle.js?ver=1.0.27"></script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-content/plugins/kallyas-addon-nav-overlay/assets/app.min.js?ver=1.0.10"></script>
		<script type="text/javascript" src="https://careernetwork.amro.io/wp-includes/js/wp-embed.min.js?ver=5.4.6"></script>
		<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<symbol id="icon-znb_close-thin" viewBox="0 0 100 100">
				<path d="m87.801 12.801c-1-1-2.6016-1-3.5 0l-33.801 33.699-34.699-34.801c-1-1-2.6016-1-3.5 0-1 1-1 2.6016 0 3.5l34.699 34.801-34.801 34.801c-1 1-1 2.6016 0 3.5 0.5 0.5 1.1016 0.69922 1.8008 0.69922s1.3008-0.19922 1.8008-0.69922l34.801-34.801 33.699 33.699c0.5 0.5 1.1016 0.69922 1.8008 0.69922 0.69922 0 1.3008-0.19922 1.8008-0.69922 1-1 1-2.6016 0-3.5l-33.801-33.699 33.699-33.699c0.89844-1 0.89844-2.6016 0-3.5z"></path>
				</symbol>
				<symbol id="icon-znb_play" viewBox="0 0 22 28">
				<path d="M21.625 14.484l-20.75 11.531c-0.484 0.266-0.875 0.031-0.875-0.516v-23c0-0.547 0.391-0.781 0.875-0.516l20.75 11.531c0.484 0.266 0.484 0.703 0 0.969z"></path>
				</symbol>
 			</defs>
		</svg>
	</body>
</html>