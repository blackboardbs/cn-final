@extends('cn.app')
@section('content')
    
<section class="zn_section eluid09739b64     section-sidemargins    zn_section--masked zn_section--relative section--no " id="eluid09739b64">

<div class="zn-bgSource ">
<div class="zn-bgSource-image" style="background-image:url(themes/cn/images/WEB_BACK2.jpg);background-repeat:no-repeat;background-position:center top;background-size:auto;background-attachment:scroll"></div>
<div class="zn-bgSource-overlayGloss"></div>
</div>
<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

    <div class="row ">
        
<div class="eluiddc928129            col-md-12 col-sm-12   znColumnElement" id="eluiddc928129">


<div class="znColumnElement-innerWrapper-eluiddc928129 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol-- tbk--colored tbk-icon-pos--before-title eluid8dd63b2d "><div class="tbk__text"><p><span style="font-size: 48px; font-family: &#39;Open Sans&#39;;"><strong><span style="color: #ffa60a;">Find the job</span> </strong><span style="color: #ffffff;">you always dreamed of.</span></span></p>
</div></div><div class="image-boxes imgbox-simple eluid032e6c0a ">
<a href="http://careernetwork.amro.io/job-search/" class="image-boxes-link imgboxes-wrapper u-mb-0  " target="_self" itemprop="url"><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="http://careernetwork.amro.io/wp-content/uploads/2019/08/SEARCH1.png" alt="" title="SEARCH1"></div></a></div>				</div>
</div>


</div>

    </div>
</div>

<div class="kl-mask kl-bottommask kl-mask--shadow_simple_down kl-mask--light"></div>		

</section>




    <section class="zn_section eluid9c1adca7    zn_ovhidden section-sidemargins    section--no " id="eluid9c1adca7">


<div class="zn_section_size full_width zn-section-height--auto zn-section-content_algn--top ">

    <div class="row gutter-0">
        
<div class="eluid15ee7e9f            col-md-6 col-sm-6   znColumnElement" id="eluid15ee7e9f">


<div class="znColumnElement-innerWrapper-eluid15ee7e9f znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					
<div class="zn_custom_container eluid67dd281a  smart-cnt--default   clearfix">
            
<div class="row zn_col_container-smart_container gutter-0">

<div class="eluid3ac886e3            col-md-12 col-sm-12   znColumnElement" id="eluid3ac886e3">


<div class="znColumnElement-innerWrapper-eluid3ac886e3 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluidacd12c72 "><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="http://careernetwork.amro.io/wp-content/uploads/2019/08/holdingresume5.jpg" alt="" title="holdingresume5"></div></div></div>				</div>
</div>


</div>
</div>
            </div><!-- /.zn_custom_container -->


            </div>
</div>


</div>

<div class="eluid174d847b            col-md-6 col-sm-6   znColumnElement" id="eluid174d847b">


<div class="znColumnElement-innerWrapper-eluid174d847b znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					
<div class="zn_custom_container eluid358eceed  smart-cnt--default   clearfix">
            
<div class="row zn_col_container-smart_container gutter-0">

<div class="eluid96cc88f8            col-md-12 col-sm-12   znColumnElement" id="eluid96cc88f8">


<div class="znColumnElement-innerWrapper-eluid96cc88f8 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol-- tbk--colored tbk-icon-pos--before-title eluid9662b348 "><div class="tbk__text"><p><span style="font-family: &#39;Open Sans&#39;; font-size: 32px;">Build your resume for <strong>FREE</strong> and find your dream job.</span></p>
</div></div><div id="eluid2a1e257c" class="zn_buttons_element eluid2a1e257c text-left "><a href="http://careernetwork.amro.io/cv-builder/" id="eluid2a1e257c0" class="eluid2a1e257c0 btn-element btn-element-0 btn  btn-fullcolor btn-custom-color btn-md  zn_dummy_value btn-icon--before btn--rounded" target="_self" itemprop="url"><span>BUILD YOUR RESUME NOW</span></a></div>				</div>
</div>


</div>
</div>
            </div><!-- /.zn_custom_container -->


            </div>
</div>


</div>

    </div>
</div>

        </section>


    <section class="zn_section eluid5b07051d     section-sidemargins    zn_section--masked zn_section--relative section--no " id="eluid5b07051d">


<div class="zn_section_size full_width zn-section-height--auto zn-section-content_algn--top ">

    <div class="row gutter-0">
        
<div class="eluidf28077a0            col-md-6 col-sm-6   znColumnElement" id="eluidf28077a0">


<div class="znColumnElement-innerWrapper-eluidf28077a0 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					
<div class="zn_custom_container eluid9d019f57  smart-cnt--default   clearfix">
            
<div class="row zn_col_container-smart_container gutter-0">

<div class="eluid535ba5a8            col-md-12 col-sm-12   znColumnElement" id="eluid535ba5a8">


<div class="znColumnElement-innerWrapper-eluid535ba5a8 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid92d7b203   kl-iconbox--type-img  kl-iconbox--fright kl-iconbox--align-right text-right kl-iconbox--theme-light element-scheme--light" id="eluid92d7b203">
<div class="kl-iconbox__inner clearfix">


    <div class="kl-iconbox__icon-wrapper ">
<img class="kl-iconbox__icon" src="http://careernetwork.amro.io/wp-content/uploads/2015/11/ico-world.svg" alt="" title="ico-world">		</div><!-- /.kl-iconbox__icon-wrapper -->


<div class="kl-iconbox__content-wrapper">


<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
    <h3 class="kl-iconbox__title element-scheme__hdg1" itemprop="headline">I AM A JOBSEEKER</h3>
</div>
            <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
    <p class="kl-iconbox__desc">Create your professional resume with an online resume
builder and start applying for the best jobs.</p>			</div>


</div><!-- /.kl-iconbox__content-wrapper -->

</div>
</div>

    </div>
</div>


</div>
</div>
            </div><!-- /.zn_custom_container -->


            </div>
</div>


</div>

<div class="eluid54dac3ad            col-md-6 col-sm-6   znColumnElement" id="eluid54dac3ad">


<div class="znColumnElement-innerWrapper-eluid54dac3ad znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					
<div class="zn_custom_container eluid7c349293  smart-cnt--default   clearfix">
            
<div class="row zn_col_container-smart_container gutter-0">

<div class="eluid918ba039            col-md-12 col-sm-12   znColumnElement" id="eluid918ba039">


<div class="znColumnElement-innerWrapper-eluid918ba039 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					<div class="kl-iconbox eluidcc085ac1   kl-iconbox--type-img  kl-iconbox--fleft kl-iconbox--align-left text-left kl-iconbox--theme-light element-scheme--light" id="eluidcc085ac1">
<div class="kl-iconbox__inner clearfix">


    <div class="kl-iconbox__icon-wrapper ">
<img class="kl-iconbox__icon" src="http://careernetwork.amro.io/wp-content/uploads/2015/11/ico-results.svg" alt="" title="ico-results">		</div><!-- /.kl-iconbox__icon-wrapper -->


<div class="kl-iconbox__content-wrapper">


<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
    <h3 class="kl-iconbox__title element-scheme__hdg1" itemprop="headline">I AM AN EMPLOYER</h3>
</div>
            <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
    <p class="kl-iconbox__desc">Job posting and online resume database search service
that helps you find the best talent.</p>			</div>


</div><!-- /.kl-iconbox__content-wrapper -->

</div>
</div>

    </div>
</div>


</div>
</div>
            </div><!-- /.zn_custom_container -->


            </div>
</div>


</div>

    </div>
</div>

<div class="kl-mask kl-topmask kl-mask--shadow_simple kl-mask--light"></div><div class="kl-mask kl-bottommask kl-mask--mask3 kl-mask--light">
<svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
<feoffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feoffset>
<fegaussianblur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></fegaussianblur>
<fecomposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></fecomposite>
<fecolormatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></fecolormatrix>
<femerge>
    <femergenode in="SourceGraphic"></femergenode>
    <femergenode in="shadowMatrixInner1"></femergenode>
</femerge>
</filter>
</defs>
<path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5" style="fill:"></path>
</svg>
<i class="glyphicon glyphicon-chevron-down"></i>
</div>		
</section>


    <section class="zn_section eluid74cd1ea6     section-sidemargins    section--no " id="our-team">


<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

    <div class="row ">
        
<div class="eluiddaa5dc15            col-md-12 col-sm-12   znColumnElement" id="eluiddaa5dc15">


<div class="znColumnElement-innerWrapper-eluiddaa5dc15 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--line tbk--colored tbk-icon-pos--after-subtitle eluidfd504884 "><h4 class="tbk__title" itemprop="headline">Popular Searches</h4><span class="tbk__symbol "><span></span></span></div>				</div>
</div>


</div>

<div class="eluidffaf0426            col-md-12 col-sm-12  col-lg-6 znColumnElement" id="eluidffaf0426">


<div class="znColumnElement-innerWrapper-eluidffaf0426 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">					
<div class="zn_custom_container eluid4cd1bb94  smart-cnt--default   clearfix">
            
<div class="row zn_col_container-smart_container ">

<div class="eluid572860bd            col-md-12 col-sm-12   znColumnElement" id="eluid572860bd">


<div class="znColumnElement-innerWrapper-eluid572860bd znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

    <div class="znColumnElement-innerContent">									</div>
</div>


</div>
</div>
            </div><!-- /.zn_custom_container -->


            </div>
</div>


</div>

    </div>
</div>

        </section>


@endsection